package org.hanyuu.net;

import java.util.ArrayList;

/**
 * Created by Hanyuusha on 20.11.2016.
 */
public class Messanger {

    private static ArrayList<Client> clients = new ArrayList<Client>();

    synchronized public static void addClient(Client client) {
        clients.add(client);
    }

    synchronized public static void broudCast(ChatMessage message) {
        for (Client c : clients) {
            try {
                if (c.isConnected()) {
                    c.send(message);
                } else {
                    removeClient(c);
                }
            } catch (Exception e) {
                removeClient(c);
                e.printStackTrace();
            }
        }
    }

    synchronized static public void removeClient(Client client){
        try{
            client.getsInput().close();
            client.getsOutput().close();
            client.getSocket().close();
            clients.remove(client);
        }catch (Exception e){
            e.printStackTrace();
        }

    }


}
