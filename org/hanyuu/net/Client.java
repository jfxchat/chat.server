package org.hanyuu.net;
import java.io.*;
import java.net.*;
import java.util.*;
import org.hanyuu.Crypto;

/**
 * Created by Hanyuusha on 20.11.2016.
 */
public class Client extends Thread{
    private Socket socket;
    /*private BufferedReader sInput;
    private PrintWriter sOutput;*/
    private ObjectInputStream sInput;
    private ObjectOutputStream sOutput;
    private Crypto crypto;
    public Client(Socket socket){
        try {
            this.socket = socket;
            this.sOutput = new ObjectOutputStream(this.getSocket().getOutputStream());
            this.sInput = new ObjectInputStream(this.getSocket().getInputStream());
            this.start();
            }catch (SocketException se)
            {
                Messanger.removeClient(this);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    @Override
    public void run(){
        try{
            while (true){
                ChatMessage message = (ChatMessage)this.getsInput().readObject();
                /*Long a=0L, g=0L, p=0L;
                switch(message.code){

                    case 0:
                        a = Long.getLong(message.message);
                        break;
                    case 1:
                        g = Long.getLong(message.message);
                        break;
                    case 2:
                        p = Long.getLong(message.message);
                        break;
                    case 3:
                        this.crypto = new Crypto(a,g,p);
                }*/
                Messanger.broudCast(message);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void send(ChatMessage message){
        try {
             this.getsOutput().writeObject(message);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public boolean isConnected(){
        return this.getSocket().isConnected();
    }

    public Socket getSocket() {
        return socket;
    }

    public ObjectInputStream getsInput() {
        return sInput;
    }

    public ObjectOutputStream getsOutput() {
        return sOutput;
    }
}


