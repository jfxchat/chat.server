package org.hanyuu;
import org.hanyuu.net.Client;
import org.hanyuu.net.Messanger;

import java.net.*;
public class Main {

    public static void main(String[] args)throws Exception {
        ServerSocket ss = new ServerSocket();

        ss.bind(new InetSocketAddress("localhost",55));

        while (true) {
            Socket s = ss.accept();
            Client client = new  Client(s);
            Messanger.addClient(client);

        }
    }
}
